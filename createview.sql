CREATE VIEW ManageEmployee AS
SELECT e.EmployeeID , e.EmployeeName,
        e.EmployeeSurname, d.DeptName,
        m.ManagerName, m.ManagerSurname
FROM Employee AS e
LEFT JOIN (Department AS d, Manager AS m)
ON (e.DeptID=d.DeptID AND e.ManagerID=m.ManagerID);

